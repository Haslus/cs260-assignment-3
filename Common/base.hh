/**
* @file		base.hh
* @date 	11/11/2018
* @author	Asier Bilbao
* @par		Login: asier.b
* @par		Course: CS330
* @par		Assignment #3
* @brief 	Implementation of the client side with UDP
*/
#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN

#include <string>
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

#include <fstream> // ofstream, write file
#include <vector>

 // link the winsock library
#pragma comment (lib, "ws2_32.lib")

 // Size of network buffer
static const int BUFFER_SIZE = 1 << 13;

struct EndPoint
{
	EndPoint(short port, std::string IP = std::string())
	{
		m_address.sin_family = AF_INET;
		m_address.sin_port = htons(port);
		m_port = port;

		if (IP.empty() || IP == "local")
		{
			hostent * localhost = gethostbyname("");
			// get 1st address from hostent -> cast to in_addr -> dereference -> convert to string
			char *localIP = inet_ntoa(*(reinterpret_cast<in_addr*>(*localhost->h_addr_list)));
			// Create local endpoint info
			m_address.sin_addr.S_un.S_addr = inet_addr(localIP);
			m_IP = localIP;
		}

		else
		{
			m_address.sin_addr.S_un.S_addr = inet_addr(IP.c_str());
			m_IP = IP;
		}


	}


	sockaddr_in		m_address;
	short			m_port;
	std::string		m_IP;

};

namespace common
{
	// Prints the last (winsock) error and cleans up the library
	int cleanup()
	{
		int err = ::WSAGetLastError();

		if(err == 10054)
			return err;

		if (err)
		{
			std::cout << "Winsock error: " << err << std::endl;
			system("pause");
			return err;
		}
		
		return 0;
	}



	SOCKET create_UDP_socket()
	{
		SOCKET UDP_s = socket(AF_INET, SOCK_DGRAM, 0);
		if (UDP_s == INVALID_SOCKET)
			cleanup();

		return UDP_s;
	}

	SOCKET create_unblocking_UDP_socket()
	{
		SOCKET UDP_s = socket(AF_INET, SOCK_DGRAM, 0);
		if (UDP_s == INVALID_SOCKET)
			cleanup();

		u_long mode = 1;

		auto res = ioctlsocket(UDP_s, FIONBIO, &mode);

		return UDP_s;
	}

	void bind_socket(SOCKET socket, EndPoint ep)
	{
		if (bind(socket, reinterpret_cast<sockaddr*>(&ep.m_address), sizeof(ep.m_address)) == SOCKET_ERROR)
			cleanup();
	}

	void send_via_UDP(SOCKET sock, const std::string & message, EndPoint ep)
	{
		const int flags = 0;

		std::string message_size = std::to_string(message.size());
		Sleep(3);
		auto bytes_sent = ::sendto(sock, message_size.c_str(), static_cast<int>(message_size.size()), 0, reinterpret_cast<sockaddr*>(&ep.m_address), static_cast<int>(sizeof(ep.m_address)));
		if (bytes_sent == SOCKET_ERROR)
			cleanup();

		Sleep(3);
		bytes_sent = ::sendto(sock, message.c_str(), static_cast<int>(message.size()), 0, reinterpret_cast<sockaddr*>(&ep.m_address), static_cast<int>(sizeof(ep.m_address)));
		// communication error
		if (bytes_sent == SOCKET_ERROR)
			cleanup();

	}

	std::string receive_via_UDP(SOCKET socket, sockaddr_in & sr_in, int sr_in_size)
	{

		const size_t size = 1 << 8;
		char message_size[size] = { 0 };
		const int flags = 0;

		//receive size of the message
		auto bytes_recv = ::recvfrom(socket, message_size, size, 0, reinterpret_cast<sockaddr*>(&sr_in), &sr_in_size);

		//allocate memory
		int true_size = std::atoi(message_size);
		char * buffer = new char[true_size + 1];
		buffer[true_size] = '\0';

		// very blocking call
		bytes_recv = ::recvfrom(socket, buffer, true_size, 0, reinterpret_cast<sockaddr*>(&sr_in), &sr_in_size);

		// communication error
		if (bytes_recv == SOCKET_ERROR)
		{
			int error = cleanup();
			if (error == 10054)
				return std::string("ERROR_10054");
		}

		std::string recv(buffer);

		delete[] buffer;

		return recv;
	}

	void send_via_TCP(SOCKET sock, const std::string & buffer)
	{
		const int flags = 0;

		std::string message_size = std::to_string(buffer.size());
		Sleep(50);
		::send(sock, message_size.c_str(), static_cast<int>(message_size.size()), flags);
		Sleep(50);

		auto bytes_sent = ::send(sock, buffer.c_str(), static_cast<int>(buffer.size()), flags);
		// communication error
		if (bytes_sent == SOCKET_ERROR)
			cleanup();
	}

	std::string receive_via_TCP(SOCKET sock)
	{
		const size_t size = 1 << 8;
		char message_size[size] = { 0 };
		const int flags = 0;

		//receive size of the message
		::recv(sock, message_size, size, flags);

		//allocate memory
		int true_size = std::atoi(message_size);
		char * buffer = new char[true_size + 1];
		buffer[true_size] = '\0';

		// very blocking call
		int bytes_recv = ::recv(sock, buffer, true_size, flags);

		// communication error
		if (bytes_recv == SOCKET_ERROR)
		{
			int error = cleanup();
			if (error == 10054)
				return std::string("ERROR_10054");
		}

		std::string recv(buffer);

		delete[] buffer;

		return recv;
	}

	SOCKET create_TCP_socket()
	{
		SOCKET TCP_s = socket(AF_INET, SOCK_STREAM, 0);
		if (TCP_s == INVALID_SOCKET)
			cleanup();

		char value = '1';
		setsockopt(TCP_s, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

		return TCP_s;

	}

	void listen_socket(SOCKET socket)
	{
		if (listen(socket, SOMAXCONN) == SOCKET_ERROR)
			cleanup();
	}

	SOCKET create_TCP_listener(EndPoint ep)
	{
		// create a TCP socket
		SOCKET listener = create_TCP_socket();
		// bind it to the local address parameter
		bind_socket(listener,ep);
		// put it to listen
		listen_socket(listener);

		return listener;
	}

	void connect(SOCKET socket, const EndPoint & server_endpoint)
	{
		if (::connect(socket, (const sockaddr *)(&server_endpoint.m_address), sizeof(server_endpoint.m_address)) == SOCKET_ERROR)
			cleanup();

	}
}

struct ConfigFile
{
	ConfigFile()
	{
		std::string line;
		std::vector<std::string> info;
		std::ifstream out("configfile.txt");
		while (getline(out, line))
		{
			info.push_back(line);
		}
		out.close();

		serverIP = info[0];
		serverPort = std::stoi(info[1]);
		clientUDPport = std::stoi(info[2]);
		directoryToStoreReceivedFiles = info[3];
		directoryToShareFilesFrom = info[4];
	}

	std::string		serverIP;
	short			serverPort;
	short			clientUDPport;
	std::string		directoryToStoreReceivedFiles;
	std::string		directoryToShareFilesFrom;
};










