You can test the assignment in the bin folder. Open the server first, then the clients. If you want more
than two clients, create a new folder and put the Client.exe and config file. Don't forget to also create folders
and change the config file accordingly.

The config file has the same structure as the one on the handout.

A user cannot send and receive at the same time.
If the server closes, no crash occurs, but the client will have to be manually closed, after finishing the 
file transmission.

/show To show all available files
/download "filename.extension" To start downloading the file


Press X to close Clients or Server.

There is a percentage to indicate how much is remaining.