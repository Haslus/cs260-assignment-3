/**
* @file		client.cpp
* @date 	11/11/2018
* @author	Asier Bilbao
* @par		Login: asier.b
* @par		Course: CS330
* @par		Assignment #3
* @brief 	Implementation of the client side with UDP and TCP
*/
#include "../Common/base.hh"
#include <thread>
const int headerSize = 15;

/**
* @brief 	Sends a list of the files from the directory to the server
* @param	SOCKET			The socket of the client
* @param	std::string		The path of the files

*/
void send_file_list(SOCKET sock, std::string path)
{

	std::vector<std::string> names;
	std::vector<std::string> filesize;
	std::string search_path = path + "\\*";


	WIN32_FIND_DATA fd;

	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);

	if (hFind != INVALID_HANDLE_VALUE) {
		do {

			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				names.push_back(fd.cFileName);
				filesize.push_back(std::to_string(fd.nFileSizeLow));
			}

		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}

	std::string packet;

	for (int i = 0; i < names.size(); i++)
	{
		packet += names[i];
		packet += ' ';
		packet += filesize[i];
		packet += '\n';
	}
	packet  += "END";

	common::send_via_TCP(sock, packet);
		
}

/**
* @brief 	Receives a file from the other user
* @param	SOCKET			The socket of the client
* @param	const char *	The filename
* @param	EndPoint		Endpoint of the receiving user
* @param	int				The size of the file
* @param	ConfigFile		Information of the user
*/
int receive_file( SOCKET s,const char *filename, int size, ConfigFile info)
{
    std::ofstream file(info.directoryToStoreReceivedFiles + "/" + filename, std::ios::binary );
    if( !file.is_open() )
        throw "Error opening file";

    // We hold the remote endpoint info here

    sockaddr_in src;
    int srcSize = sizeof(src);	

    // some necessary local variables
    char		recvBuffer[BUFFER_SIZE + headerSize] = {0};	// buffer to send
    unsigned int	bytesRecv		= 0;	// counter for total sent bytes
    int			result			= 0;	// result of the sendto
    int			chunk			= 0;	// amount of data to receive and write
	int currentAck = 0;
	int totalsize = size;
	int threshold = 5;
	std::string header;
	std::string message;
    do
    {
        // Next piece of data to read and send. Datagram size (max) or remaining data to be processed
        chunk = (size >= BUFFER_SIZE) ? BUFFER_SIZE : size;
        // Receive data
		::recvfrom(s, recvBuffer, chunk + headerSize, 0,
			reinterpret_cast<sockaddr*>(&src), &srcSize); // store the sender's endpoint in 'src'
															// we can ignore source address if not going to ack
		header = std::string(recvBuffer).substr(0,headerSize);

		if (currentAck != std::stoi(header.substr(4)))
			continue;

		currentAck++;

		message = std::string( (recvBuffer + headerSize) ,chunk);

		bytesRecv += chunk + headerSize; // increase received bytes count
							 
		file.write(message.c_str(), chunk);// Write received data to file

		// this should be agreed upon with the sender program
		if (::sendto(s, header.c_str(),headerSize, 0, reinterpret_cast<sockaddr*>(&src), sizeof(src)) == SOCKET_ERROR)
			throw "Error sending acknowledge";

		if (threshold == 5)
		{
			std::cout <<  (1 - static_cast<double>(size) / static_cast<double>(totalsize)) * 100.f << "% received." << std::endl;
			threshold = 0;
		}
		else
			threshold++;
		
		header.clear();
		message.clear();

    } while( (size -= chunk) > 0 && file.good()); // keep going until the entire file size has been sent
	std::cout << "100% received.";
	file.close();

    return bytesRecv;
}

/**
* @brief 	Send a file to the other user
* @param	const char *	The filename
* @param	SOCKET			The socket of the client
* @param	EndPoint		Endpoint of the receiving user
* @param	int				The size of the file
* @param	ConfigFile		Information of the user
*/
int send_file(const char *filename, SOCKET s, EndPoint dest, int size, ConfigFile info)
{
	// open the desired file and check its validity
	std::ifstream file(info.directoryToShareFilesFrom + "/" + filename, std::ios::binary);
	if (!file.is_open())
		throw "Error opening file";

	// some required local variables
	char		sendBuffer[BUFFER_SIZE] = { 0 };	// buffer to send
	unsigned int	bytesSent = 0;	// counter for total sent bytes
	int			result = 0;	// result of the sendto
	int			chunk = 0;	// amount of data to read and send
	int			ackNum = 0;
	//std::thread t{CheckAck,s,std::ref(dest)};
							// transfer loop
							// keep sending until there is no more of it left to be sent

							// Next piece of data to read and send. Datagram size (max) or remaining data to be processed
	int destSize = sizeof(dest.m_address);
	int totalsize = size;
	int threshold = 5;
	chunk = (size >= BUFFER_SIZE) ? BUFFER_SIZE : size;
	// Read next buffer from file
	file.read(sendBuffer, chunk);
	
	while ((!file.eof()) && (size > 0))
	{	

		char ackheader[10];

		std::string header = "ack ";
		header += _itoa(ackNum, ackheader, 10);
		header.insert(header.size(),headerSize - header.size() - 1,'-');
		header += '\n';


		std::string char_to_string = std::string(sendBuffer, chunk);
		std::string message = header;
		message += char_to_string;

	//	message.resize(chunk + headerSize);

		result = ::sendto(s, message.c_str() , chunk + headerSize, 0,
			reinterpret_cast<sockaddr*>(&dest.m_address), sizeof(dest.m_address));
		if (result == SOCKET_ERROR) // SOCKET_ERROR = -1
			throw "Error sending file";

		Sleep(5);

		char ackHeader[headerSize];

		auto ackResult = ::recvfrom(s, ackHeader, headerSize, 0, reinterpret_cast<sockaddr*>(&dest), &destSize);

		if (ackResult != -1)
		{
			int currentAck = std::stoi(std::string(ackHeader).substr(4));

			if (currentAck == ackNum)
			{
				if (threshold == 5)
				{
					std::cout << (1 - static_cast<double>(size) / static_cast<double>(totalsize)) * 100.f << "% sent." << std::endl;
					threshold = 0;
				}
				else
					threshold++;
				
				bytesSent += result; // increase sent bytes count
				size -= (result - headerSize);	// decrease remaining data count
				// Next piece of data to read and send. Datagram size (max) or remaining data to be processed
				chunk = (size >= BUFFER_SIZE) ? BUFFER_SIZE : size;
				// Read next buffer from file
				file.read(sendBuffer, chunk);
				ackNum++;
			}
		}
	}

	std::cout << "100% sent." << std::endl;
	file.close();
	return bytesSent;
}
/**
* @brief 	Send data to the server
* @param	SOCKET	The socket of the client
* @param	EndPoint	The endpoint of the user
* @param	ConfigFile	Information of the user
*/
void send_logic(SOCKET clientSocket, EndPoint User, ConfigFile info)
{
	
	do
	{
		std::string buffer;
		// read some string from the keyboard to send as message payload
		std::getline(std::cin, buffer);

		common::send_via_TCP(clientSocket, buffer);

		std::cin.clear();

	} while (true);
}
/**
* @brief 	Receive data from the server
* @param	SOCKET	The socket of the client
* @param	EndPoint	The endpoint of the user
* @param	ConfigFile	Information of the user
*/
void receive_logic(SOCKET clientSocket, EndPoint User, ConfigFile info)
{

	do
	{

		std::string buffer = common::receive_via_TCP(clientSocket);

		if (buffer == "ERROR_10054")
		{
			std::cout << "Connection lost with server." << std::endl;
			break;
		}

		if (buffer.find("FileReceive") != std::string::npos)
		{
			auto index = buffer.find(" ");
			std::string information = buffer.substr(index + 1);
			index = information.find(" ");
			std::string filename = information.substr(0, index);
			information = information.substr(index + 1);
			index = information.find(" ");
			std::string filesize = information.substr(0, index);

			SOCKET UDPSocket = common::create_UDP_socket();
			common::bind_socket(UDPSocket, User);
			std::cout << "Receiving " << filename << ". . .\n";
			receive_file(UDPSocket, filename.c_str(), std::stoi(filesize), info);

			closesocket(UDPSocket);
			std::cout << "Successfully received.\n";
		}

		else if (buffer.find("FileSend") != std::string::npos)
		{

			auto index = buffer.find(" ");
			std::string information = buffer.substr(index + 1);
			index = information.find(" ");
			std::string IP = information.substr(0, index);
			information = information.substr(index + 1);
			index = information.find(" ");
			std::string port = information.substr(0, index);
			information = information.substr(index + 1);
			index = information.find(" ");
			std::string filename = information.substr(0, index);
			information = information.substr(index + 1);
			index = information.find(" ");
			std::string filesize = information.substr(0, index);

			EndPoint receiver(std::stoi(port),IP);
			std::cout << "Sending " << filename << ". . .\n";

			SOCKET UDPSocket = common::create_unblocking_UDP_socket();
			common::bind_socket(UDPSocket, User);

			send_file(filename.c_str(), UDPSocket, receiver, std::stoi(filesize),info);

			closesocket(UDPSocket);
			std::cout << "Successfully sent.\n";
		}

		else
			std::cout << buffer << std::endl;

	} while (true);
}
/**
* @brief 	main loop
*/
int main()
{
	std::cout << "**********************" << std::endl;
	std::cout << "********CLIENT********" << std::endl;
	std::cout << "**********************" << std::endl;
	// Initialize library
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		common::cleanup();

	ConfigFile info;

	SOCKET clientSocket = common::create_TCP_socket();

	EndPoint serverEP(info.serverPort,info.serverIP);

	common::connect(clientSocket, serverEP);

	std::cout << "Connected with server." << std::endl;

	Sleep(50);

	common::send_via_TCP(clientSocket, std::to_string(info.clientUDPport));

	hostent * localhost = gethostbyname("");
	// get 1st address from hostent -> cast to in_addr -> dereference -> convert to string
	char *localIP = inet_ntoa(*(reinterpret_cast<in_addr*>(*localhost->h_addr_list)));

	Sleep(50);
	common::send_via_TCP(clientSocket,localIP);

	Sleep(50);
	send_file_list(clientSocket,info.directoryToShareFilesFrom);

	EndPoint User(info.clientUDPport);

	std::cout << "Your IP: " << localIP << std::endl;
	std::cout << "Your UDP port: " << info.clientUDPport << std::endl;

	std::thread recv{ receive_logic, clientSocket,User,info };
	send_logic(clientSocket, User,info);

	recv.join();

	shutdown(clientSocket, SD_BOTH);

	std::cin.get();
		

}

