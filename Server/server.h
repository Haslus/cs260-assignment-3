#pragma once

#include <iostream>
#include <vector>
#include <thread>

struct User
{
	int m_ID;
	std::string m_port;
	std::string m_IP;
	SOCKET m_sock;
	std::vector<std::string> m_files;
	std::vector<std::string> m_files_size;
};


std::vector<User*> Users;
std::vector<std::thread> All_threads;
int ID = 0;