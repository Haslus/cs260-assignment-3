/**
* @file		server.cpp
* @date 	11/11/2018
* @author	Asier Bilbao
* @par		Login: asier.b
* @par		Course: CS330
* @par		Assignment #3
* @brief 	Implementation of the server side for the third assignment
*/
#include "../Common/base.hh"
#include "server.h"

/**
* @brief 	Gets a file list from the user
* @param	User	The user that will give the list
*/
void get_user_files(User * user)
{

	std::string packet = common::receive_via_TCP(user->m_sock);
	std::vector<std::string> files;
	std::vector<std::string> filesizes;

	while (true)
	{
		if (packet == "END")
			break;

		std::string current_file = packet.substr(0, packet.find(' '));

		files.push_back(current_file);

		std::string current_file_size = packet.substr(packet.find(' ') + 1, packet.find('\n') - packet.find(' ') - 1);

		filesizes.push_back(current_file_size);

		packet = packet.substr(packet.find('\n') + 1);

	}
	user->m_files = files;
	user->m_files_size = filesizes;
}
/**
* @brief 	Loop for the user
* @param	User	The current user of the loop
*/
void user_logic(User * user)
{

	while (true)
	{
		std::string message = common::receive_via_TCP(user->m_sock);

		if (message == "ERROR_10054")
			break;

		if(message == "/show")
		{
			for (auto it : Users)
			{
				if (it == nullptr)
					continue;

				if(it == user)
					common::send_via_TCP(user->m_sock, "\nUser " + std::to_string(it->m_ID) + " (you):\n");
				else
					common::send_via_TCP(user->m_sock, "\nUser " + std::to_string(it->m_ID) + ':' + '\n' );

				for (int i = 0; i < it->m_files.size(); i++)
					common::send_via_TCP(user->m_sock, it->m_files[i] + " of " + it->m_files_size[i] + " bytes");


			}
		}

		if (message.find("/download") != std::string::npos)
		{
			std::string filename = message.substr(10);

			int user_index = -1;
			int file_index = -1;
			int userID = -1;

			//Check if file exists
			for (int i = 0; i < Users.size(); i++)
			{
				for (int j = 0; j < Users[i]->m_files.size(); j++)
				{
					if (Users[i]->m_files[j] == filename)
					{
						user_index = i;
						file_index = j;
						userID = Users[i]->m_ID;
						break;
					}
				}
			}

			if (userID == user->m_ID)
			{
				common::send_via_TCP(user->m_sock, "You own that file.");
				continue;
			}
			

			if (user_index == -1 || file_index == -1)
			{
				common::send_via_TCP(user->m_sock, "File not found.");
				continue;
			}

			common::send_via_TCP(user->m_sock,"FileReceive " + filename + " " + Users[user_index]->m_files_size[file_index]);

			common::send_via_TCP(Users[user_index]->m_sock, "FileSend " + user->m_IP + " " + user->m_port + " "
				+ filename + " " + Users[user_index]->m_files_size[file_index]);


				

		}
	}

	for (int i = 0; i < Users.size(); i++)
	{
		if (Users[i] == user)
		{
			std::cout << "User " << std::to_string(user->m_ID) << " has disconnected." << std::endl;
			Users[i] = nullptr;
			shutdown(user->m_sock, SD_BOTH);
			delete user;
			break;
		}
	}

	
}

/**
* @brief 	Waits for a new client to connect
* @param	SOCKET		Socket that is listening
* @return	A newly connected client
*/
User * wait_for_new_connection(SOCKET listener)
{	

	SOCKET userSocket = accept(listener, NULL, NULL);
	User * temp = new User();
	std::cout << "Creating User Info..." << std::endl;
	temp->m_sock = userSocket;
	temp->m_port = common::receive_via_TCP(userSocket);
	temp->m_IP = common::receive_via_TCP(userSocket);
	temp->m_ID = ID;
	std::cout << "Receiving file list.." << std::endl;
	get_user_files(temp);

	ID++;

	std::cout << "User " << std::to_string(temp->m_ID) << " has connected." << std::endl;

	return temp;

}
/**
* @brief 	main function
*/
int main()
{
	std::cout << "**********************" << std::endl;
	std::cout << "********SERVER********" << std::endl;
	std::cout << "**********************" << std::endl;

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		common::cleanup();

	ConfigFile info;

	EndPoint serverEP(info.serverPort, info.serverIP);

	SOCKET serverSocket = common::create_TCP_listener(serverEP);

	std::cout << "Server Open on:" << std::endl;
	std::cout << "Port:" << serverEP.m_port << std::endl;
	std::cout << "IP:" << serverEP.m_IP << std::endl;

	while(true)
	{
		User * user = wait_for_new_connection(serverSocket);
		All_threads.push_back(std::thread{ user_logic, std::ref(user) });
		Users.push_back(user);
	}


	for (auto it : Users)
	{
		if (it != nullptr)
		{
			shutdown(it->m_sock, SD_BOTH);
			delete it;
		}
	}

	shutdown(serverSocket, SD_BOTH);

	std::cin.get();

}
